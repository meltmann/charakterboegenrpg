# Verhalten

- muss alles, wirklich alles, in die Hand nehmen
- sammelt alles Mögliche (Nahrungsmittel, widerliches Zeugs, Kleinkram, Pulver, Tierknochen…) in seinen Manteltaschen
- ist unglaublich neugierig
- isst gelegentlich aus seinen Manteltaschen
- isst gelegentlich aus Mülltonnen (a.k.a. "Containern")
- ist ein sehr sozialer Mensch
- wirkt eher schüchtern und zurückhaltend
