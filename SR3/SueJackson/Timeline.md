Die Jahreszahlen geben Hinweise für Eckpunkte und Ereignisse, die mit der Timeline aus dem Shadowrun 3.01D Regelwerk angereichert werden könnten.

# 2011 — 2016
## Geburt und Kleinkind

Am Morgen des 11. November 2011 um 04:11 Ortszeit erblickte eine kleine Elfin das Licht der Welt in einem kleinen Seattler Krankenhaus der Transys Corporation.  
Ihr Vater _Jhon_ Mikael Jackson (genannt Jhonny, leidenschaftlicher Johnny Cash Karaoke-Sänger und Hauptverantwortlicher für den Rufnamen Sue…) war Chefarzt der Neurochirurgie im ihrem Geburts-Krankenhaus. 
Ihre Mutter Jane _Diane_ Jackson (geborene Watson) arbeitete als Oberärztin der Anästhesie ebenfalls in der selben Klinik.

Susann war insofern etwas Besonderes, als dass sie nicht nur eine der ersten aufgezeichneten Elfen war.
Sie war genetisch ein absolut eindeutiger Nachfahre zweier Menschen.
Die DNA-Analysen von Transys waren weltweit spitze und ließen auch nach mehrmaliger Prüfung keinen anderen Schluss zu, als dass die Vereinigung zweier Menschen eine Elfin zur Folge hatte.
Es war ungefähr zu der Zeit, als dieses Phänomen mit der Abkürzung *UGE* für *Ungeklärte Genetische Expression* in der Fachliteratur beschrieben wurde und etwas mehr als einen Monat vor "dem Erwachen" des Ryumyo.

Entsprechend scheuten sich Jhon und Diane, die kleine _Sue_ in eine gewöhnliche Kinderbetreuung zu geben.  
Da ihr Gehalt entsprechend hoch war, konnten sie sich eine private Sonderbetreuung für Susann leisten.
Genau genommen wuchs die Kleine im Krankenhaus auf.
Die Sonderbetreuung fand in einem umgebauten Besprechungsraum statt, sie teilte sich das Schlafzimmer mit den Nachtschwestern und Notärzten.  
Weltweit häuften sich die Berichte über das Erscheinen von Metamenschen und viele Hebammen und Kinderärzte betrachteten Susann als Studienobjekt für ihre persönliche und fachliche Weiterbildung.

# 2017 — 2029
## Grundschule 2017–2020

Um und bei 2017 erreichte das öffentliche Interesse an Meta-Menschen einen zweifelhaften Höhepunkt und als durchsickerte, dass im St. Maria Hospital in Seattle eine mittlerweile fünfjährige Elfe versteckt gehalten würde, rief das eine beachtliche Menge an Schaulustigen, Verschwörungssuchern und allgemeinen Klatschreportern auf den Plan.

Zu der Zeit beschloss Transys, die kleine Sue aus dem Fokus der Öffentlichkeit zu nehmen und in ein abgeschiedenes Labor zur Erforschung von Meta-Menschen umzusiedeln.  
Dieses Labor wurde Jhon und Diane als "humanmedizinisches Internat" präsentiert, doch jegliche Versuche in den Kontakt mit ihrer Tochter zu treten, wurde seitens des Konzerns mit Hinweis auf die Vorschriften und Geheimhaltungsstufen unterbunden.

Susann hingegen wurde auf Nachfrage zu ihren Eltern immer wieder erzählt, dass sie weggegeben wurde.
Je nach Quelle waren ihre Eltern entweder beides Menschen und sie das Resultat einer außerehelichen unreinen Vereinigung ihrer Mutter mit einem Elfen, oder ihr Vater hätte seine Ehefrau (oder Freundin) mit einer Elfenschlampe betrogen (oder vögelte fröhlich alles, das nicht bei drei auf den Bäumen war – inklusive besagter Elfenschlampe), die sie dann zufällig zur Welt brachte und diese Schande vor ihrem Elfen-Clan nicht rechtfertigen wollte.  
Sue war zu dem Zeitpunkt noch viel zu klein, um die fachliche Inkorrektheit dieser Aussagen feststellen zu können.

## Weiterführende Schule 2021–2027

In Bezug auf die Aus- und Fortbildung erfüllte das mittlerweile als "metahumanmedizinischze Internat" bezeichnete Forschungslabor alle Auflagen.  
Sie lernte zusammen mit anderen "Grotesken" wie Orks, Zwergen oder Trollen die Naturgesetze:
Biologie, Mathematik, Physik und Chemie standen ganz weit oben im Lehrplan, später folgten Biotech und Cybertech.  
Die mittlerweile gar nicht mehr so Kleinen lernten weit bevor es hip wurde, wie die ersten Prototypen der Cyberware funktionierten, welche Abstoßungsreaktionen sie hervorrufen konnten, wie dem beizukommen war und so weiter und so fort.

Weniger vorbildlich hingegen war die pädagogische Ausbildung der erstaunlicherweise ausschließlich menschlichen Mitarbeiter:innen dieses metahumanmedizinischen Internats.  
So galt die Prügelstrafe als Motivationsmittel für besonders dumme Exemplare (hauptsächlich Orks und Trolle) als durchaus probates Erziehungsmittel, obwohl diese für rein menschliche Kinder bereits seit einem Viertel Jahrhundert als unzulässig und rechtlich zu ahnden galt.

Auch die Sonderbehandlung ausschließlich weiblicher Elfen für die "praktische Einführung in geschlechtlicher Fortpflanzung" durch sehr ambitionierte männliche Mitarbeiter dürfte gemäß der damals geltenden Rechtsprechung irgendwo zwischen "Nötigung", "Missbrauch" und "Vergewaltigung" anzusiedeln sein.  
Praktischerweise wussten die Schüler:innen nichts davon, denn unsinnige Fächer wie Politik, Sozialkunde, Wirtschaft und Recht fanden auf dem Lehrplan der Einrichtung einfach nicht statt.
Natürlich hatten alle das Gefühl, die Behandlungen seien unfair, teils respektlos und ganz sicher alles Andere als angenehm, aber sie lernten früh und schmerzhaft, dass Vorschrift nun mal Vorschrift ist.  
Außerdem waren alle Bereiche inklusive der Umkleide-, Dusch- und Schlafräume zunächst durch stationäre Kameras, später durch Drohnen dauerhaft videoüberwacht.
Es war also technisch gar nicht möglich, dass hier irgendwas nicht mit rechten Dingen zu ging.

Wenngleich ein Erlebnis im Spätherbst 2026 kurz vor Susanns 15. Geburtstag ihr das Gegenteil suggerierte.  
Mittlerweile hatte sie festgestellt, dass sie sich eher dem weiblichen Geschlecht zugewandt fühlt.
Unglücklicherweise hatte sie das auch ihrem Gynäkologen bei einer Routineuntersuchung erzählt.
Prompt fühlten sich einige sehr ambitionierte junge Herren, die ihr freiwilliges soziales Jahr im Dienste der Wissenschaft abhielten, motiviert und berufen Susann wieder einmal von den Vorzügen der getrennt geschlechtlichen Vereinigung zu überzeugen.
Sofern es notwendig werden würde auch gegen ihren Willen…

Es wurde notwendig. Obwohl es 6 gegen 1 stand brach sie zwei Herren erfolgreich das Nasenbein und zertrümmerte einem von ihnen ein paar Rippen, bevor es den Jungs gelang, sie mit Seilen an den Schultern um einen Pfeiler zu fixieren.  
Was darauf hin folgte wissen die Wenigsten, da unerklärlicherweise die Videoaufzeichnungen durch die Drohnen an diesem Punkt aussetzen…

Etwa eine halbe Stunde später setzen die Aufnahmen wieder ein, Susanns rechter Arm ist bereits besorgniserregend blauschwarz verfärbt und eine ältere Frau mit Laborkittel, schwarz gerahmter Brille und ordentlichem grauen Dutt reißt energisch die Tür auf. Ihr Name ist nicht zu erkennen, doch in ihrem Laborkittel befindet sich nur ein einziger Kugelschreiber.  
Anscheinend wurde die Prügelstrafe auch für Menschen wieder eingeführt, einige Nasen scheinen zu brechen und Kiefer wirken ausgehakt.
Eindeutige Fußberührungen an einige Gluteus Maximus der Herren deuten auf eine wenig freundliche Beendigung der Aktivitäten hin.

Frau Prof. Dr. Agnes Fleischhauer, wie ihr Namensschild jetzt der Kamera verrät, bellt einige Kommandos ins Wandtelefon, bevor sie Susanns Fesseln los schneidet.
Sie macht sich gar nicht erst die Mühe, die Elfe wieder anzuziehen, sondern reißt ihr die letzten Kleidungsfetzen vom Leib und wickelt sie in ein Operationshemdchen.  
Kurz darauf hechten zwei Männer mit Nottrage durch die Tür, hieven die mittlerweile bewusstlos zusammengebrochene Elfe hinauf und sprinten mitsamt Gepäck wieder hinaus.

Es folgt eine fast 12-stündige Operation, zunächst zur Rettung des Armes und nach etwa drei Stunden zwecks Anbringung eines Cyberarms nach aktuellem Forschungsstand. 
Kurzerhand wurde also entschieden, die metahumanmedizinische Machbarkeit derartiger Installationen unter Ausweitung der Gesetzeslage zur Volljährigkeit zu testen.  
Das Vernähen der Blut- und Nervenbahnen geschah damals (es ist immer noch 2026…) mit Skalpellen, Nähnadeln und ähnlichen Instrumenten aus Chirurgenstahl, was nach heutigem Maßstab als absolut unpräzise und grobschlächtig angesehen werden kann. 
Die Qualität der Installation ist unter diesem Aspekt also absolut erstaunlich.

Unglücklicherweise bekamen auch die besten Therapeuten Transys' das seelische Trauma aus der Elfe nicht wieder heraus therapiert.  
Ihr Arm erinnert sie fortwährend an die Grausamkeit der Menschen und die Unzuverlässigkeit der so genannten Überwachungstechnik.

Es bedurfte mehrere Stunden intensivster Psychotherapie, um Susann intrinsisch zur Aufnahme eines Dualen Studiums als Unfallsanitäterin zu begeistern.
Schlussendlich war es dann aber doch erfolgreich, woran historische Aufzeichnungen über Menschen mit nekrotischen Extremitäten und deren Gnadentod bzw. später komplette soziale Ausgestoßenheit nach Amputation einen erheblichen Einfluss gehabt haben dürften.

## Duales Studium 2028–2033

Dank ihrer Interessen an den Naturwissenschaften machte sie während ihres dualen Studiums zur Rettungssanitäterin fachlich eine hervorragende Figur.
Sowohl das theoretische Wissen als auch die praktischen Aufgaben gingen ihr erstaunlich mühelos und präzise von der Hand.

Sie verstand die Zusammenhänge von Erregern und Infektionen, die Notwendigkeit von Infektionsschutz und die Maßnahmen zur Erregerbekämpfung, Zusammensetzung und Wirkung von Betäubungsmitteln und die Auswirkungen kleinster Ungenauigkeiten auf das komplexe Gesamtsystem "Organismus".  
Weiterhin meisterte sie Skalpellschnitte, Näharbeiten, Frakturkorrekturen und Gewebeentnahmen mühelos.  
Ihr persönliches Interesse galt der Cyberware der 20er Jahre, die Sue damals liebevoll "Biomechanik" nannte.
Entsprechend interessierte sie sich auch für die physikalischen Konzepte der Mechanik, aber auch Optik und Thermik fand sie ausgesprochen interessant und lehrreich.

Aus Gründen der Geheimhaltung unterhielt das Labor einen eigenen kleinen Fuhrpark mit entsprechenden Instandhaltungseinrichtungen und Sue verdiente sich in der vorlesungsfreien Zeit etwas hinzu, indem sie den Mechaniker:innen zur Hand ging.
Dass sie diesen Schritt in erster Linie gegangen war, weil sie ein Auge auf die Chefmechanikerin Rebecca Cunningham geworfen hatte, behielt sie allerdings für sich.

Mit der Zeit lernte sie quasi nebenbei auch die Mechanik und Elektronik der Rettungswagen und Kurier-Leichtkrafträder kennen und lieben und wurde die fähigste Unterstützung Rebeccas.

# 2034 — 2039

## Der erste Job

Nach ihrem Studium, dass sie mit einem Doktortitel abgeschlossen hatte, ließ sie sich mit Empfehlung in die Redmond Barrens versetzen.  
Dort begann ihre Karriere als Assistenzärztin eines Rettungssanitäters.
Sie brillierte nicht nur mit ihren Fähigkeiten als Ärztin und Vertriebstalenten für Cyberimplantate auf dem Wagen, die ihren Vorgesetzten erstaunlich gelassen ließen; sie sparte dem Fuhrpark auch etliche Tausend Nuyen, weil sie kleinere und größere Reparaturarbeiten an den Fahrzeugen kurzerhand selbst erledigte.

In den letzten Jahren wuchs allerdings nicht nur das Aufkommen neuer Metamenschen durch die Auswirkungen der Goblinisierung stetig an, auch der Rassismus gegenüber den "anderen" Homo Sapiens machte sich breit.  
So insistierten mehrere Schwerverletzte, dass sie als echte *homo sapiens sapiens* doch bitte das Recht auf eine Behandlung von **Menschen** hätten und nicht wie der niedere Pöbel von *Abschaum* betatscht werden wollten.

Als sie mit ihrem Oberarzt nach Dienst darüber reden wollte, fiel ihr dann auf, warum er, wie auf dem Flur gemunkelt wurde "anscheinend keine Befürchtung hätte, dass die Spitzohrschlampe ihm den Rang abläuft" — er lehnte komplett erschlafft mit einer leeren Flasche Whiskey vor sich völlig desorientiert auf einem Stuhl, kaum fähig ein gerades Wort herauszubringen.

In dieser Zeit lernte sie etwas mehr über die Geschichte Seattles und der Metamenschen und ihr wurde bewusst, dass sie rein biologisch betrachtet keinesfalls ein "Verkehrsunfall" sein konnte: In ihrem Geburtsjahr wurden Elfen und Zwerge das erste Mal überhaupt erwähnt, Orks und Trolle durch die Goblinisierung kamen noch viel später dazu.  
Später ließ ihr "ein Freund" (mittlerweile vermutet sie eher "ein Neider") streng geheime Unternehmensaufzeichnungen zukommen, die über die Anfangsphase des Labors berichtet. Unter Anderem wird dort einem gewissen Paar "Jhon und Diane Jackson" ein internes Disziplinarverfahren an den Hals gehängt, da sie Stein und Bein behaupten als menschliches Paar eine Elfe zur Welt gebracht zu haben, die ihnen vom Konzern angeblich entwendet und zwecks Experimenten in ein geheimes Labor eingeschleust worden sei.
Selbstverständlich endet dieser Prozess wie alle Verfahren, in denen irgendwelche Verschwörungs-Schwurbler irgendwelche Post-Faktischen Experiment-Weltverschwörungen irgendwelcher Konzerne aus dem Ärmel schütteln, nur weil sie mit ihrem Gehalt oder Vorgesetzten unzufrieden sind: Schadenersatz- und Unterlassungsklage mit sofortiger Freistellung und Verbot der weiteren Berufsausübung.

Alles in Allem stellt Sue fest, dass ihr diese so genannten "Menschen" zu den Spitzohren raus hängen und Konzerne ein derart verlogener Haufen Drek sind, dass sie absolut keinen Bock mehr auf diese Penner hat.

## Der Bruch mit Transys

Mittlerweile wird ihr das alles zu viel und sie beschließt ihrem mal wieder völlig besoffenen Oberarzt Chuck Harris mitzuteilen, dass sie aussteigen will.  
Entgegen ihrer Erwartung scheint er schlagartig nüchtern und bereit, sie in jedem Fall zu unterstützen.
Misstrauen macht sich in ihr breit.  
Sollte es tatsächlich vernünftige Menschen geben? 
Hat er auch genug von dem Laden und nur, typisch Pelzträger, keine Eier selbst etwas zu unternehmen?
Oder sieht das Äffchen in ihrem Weggang lediglich einen Aufschub seines besiegelten Karriere-Aus durch fähige Jungärzte?

Einerlei, was er ihr mitzuteilen hatte klang schlüssig.  
Da sie ein PISS besaß, konnte sie überall nachverfolgt werden.
Bei dem PISS oder "Probanden-Identifikations-Signatur-Sender" handelte es sich um ein Cyberimplantat, dessen Hauptaufgabe es war, nun ja, Probanden eindeutig zu identifizieren.
Aus ihm ging dann 2036 der SIN-Chip hervor, den sich Transys teuer von der UCAS bezahlen ließ.  
Die Technik des PISS war verhältnismäßig anfällig, so dass Probanden zusätzlich noch eine PISP in den Nacken eingebrannt wurde: die Probanden-Identifikations-Signatur-Prüfsumme.
Offiziell entsprach sie den Vorgaben des Datenschutzes, da gewisse Konstellationen von Alter, Geschlecht, Rasse und Geburtsdatum rein technisch zur selben PIPS führen konnten und entsprechend keine Rückschlüsse auf die PISS zuließen.
In der Praxis trat das aber nicht auf, so dass alle Probanden eindeutig an ihrem Nacken-Branding zu erkennen waren.
Das hatte natürlich Vorteile, wenn die PISS-Lesegeräte mal nicht so wollten wie die Mitarbeiter:innen.  
Bevor sie sich also gegen den Konzern stellte, musste sie diese Identifikationsmerkmale also möglichst loswerden.

Beide hielten sich vermehrt in Gebieten von Motorradclubs auf und hofften auf unidentifizierbare Elfen-Überreste. 
Chuck wusste, dass bei Metamenschen selbst bei begründetem Verdacht quasi nie DNS Analysen durchgeführt wurden.
Bei Gangs und Clubs wurde das ebenfalls unterlassen, weil sie oftmals nicht einmal vollwertiger Teil der Gesellschaft waren oder sich niemand wirklich ernsthaft mit ihren Gegnern anlegen wollte.
Dementsprechend waren Verkehrsopfer der Motorradclubs gleich doppelt uninteressant: Gesellschaftlich irrelevant bei lebensgefährlichen Aktivitäten.

Einer schönen Samstag Nacht im April 2037 war es dann soweit: Eine Schrotflinte hatte einen Elf des ansässigen Motorradclubs "New World Order" quasi auf Gesichtshöhe von der Harley geholt.  
Seine Lederkluft war ein wenig zu groß für Sue, was sie allerdings nicht davon abhielt, diese über den Transys Techniker Overall zu ziehen.  
Chuck operierte den PISS (Probanden-Identifikations-Signatur-Sender) von Susann Jackson, geboren am 11.11.2011 um 11:11 UTC in einen fleischigen Teil, der wohl mal der Nacken des Opfers gewesen sein mochte.
Wobei "operieren" geprahlt ist, "reinquetschen" trifft es eher.
Sue verarztete er mit etwas Sprühflaster und riet ihr, sich die in ihrem Nacken eingebrannte PISP (Probanden-Identifikations-Signatur-Prüfsumme) schleunigst überstechen zu lassen.  
Sie setzte sich mit dem verbeulten Hobel ab, während Chuck einen Großteil des Elf-Biker-Puzzles in den Wagen lud.

Er wollte einen Bericht verfassen, gemäß dessen Sue bei einer Schießerei in den Barrens ums Leben gekommen sei und sie konnte in Freiheit leben.

## Der Ruf der Straße

Da stand sie nun, in einer viel zu großen blutgetränkten geklauten Lederkombi mit nichts als einem unpersonalisierten Cred Stick und einer von der New World Order geklauten Harley-Davidson in den Straßen Seattles.

Vielleicht war es Ehrgefühl, vielleicht war es Panik, vielleicht auch nur reine Planlosigkeit, die sie veranlasste, nach Spuren dieses Motorradclubs zu suchen und sie über den Tod ihres Mitglieds zu informieren.  
Wie so oft fand nicht sie den Club, sondern der Club fand sie.
Aus unerklärlichen Gründen ging die Sache erstaunlich wenig hässlich für sie aus, als sie sichtlich erleichtert die inzwischen gereinigte Lederkombi auf die Harley packte und von der Maschine zurück trat.
Dass sie dabei erklärte, sie hätte nebenbei den Vergaser optimiert, den Motor gereinigt, die Chromteile auf Hochglanz poliert und die Bremsen justiert, schien positive Auswirkungen auf die Gesamtsituation gehabt zu haben.  
Eventuell hatte der mittlerweile an ihre neuen körperlichen Dimensionen einer ausgewachsenen Elfe angepasste High-End-Chrom-Cyberarm und die lässig in den Gürtel geschobene .357er Glock einen weiteren Teil zur Gesamtsituation beigetragen.

Als einer der Drei zur Harley schlurfte, merkte sie an, dass sie sich mal sein Bein anschauen könne.
Ein knurrendes "Gibt's nix zu schaun, is ne verdammte .45er im Hüftknochen!" entgegnete sie mit einem erstaunten "Dann kann ich das Ding doch rausholen!".
Das ihr entgegenschallenden Gelächter verwirrte sie umso mehr, was wiederum die lachenden Biker verwirrte.  
Kurzerhand führten sie eine Extraktion durch und brachten sie mit verbundenen Augen zu ihrem Lazarett.
Die Beretta an Susanns Kopf sollte sie daran erinnern, dass sie den Mund besser nicht zu voll genommen haben sollte, als sie mit den Operationsvorbereitungen begann.  
Verbesserungsvorschläge zur Qualität der Hygiene und Ausrüstung dieses bestenfalls als Feldlazarett zu bezeichnenden Saustalls wurden irgendwie ignoriert, ihre Überzeugungskraft reichte lediglich für die Genehmigung zur ausladenden Desinfektion ihrer Hände und Arme sowie einem Paar Einmalhandschuhe. Wider Erwarten für die New World Order und ihrem Tierarzt/Schlachter-Sanitäter brauchte sie nur wenige Minuten, um das Projektil aus dem Knochen zu holen und die Wunde zu nähen.
Sprühpflaster gab es keine, auch kein Jod oder ähnliches hochwertiges Material.
So blieb ihr nichts Anderes übrig, als Pflaster aus handelsüblichen KFZ Verbandskästen drüber zu kleben und dem Patienten einzutrichtern, das Ding täglich zu erneuern und vorher mit Hochprozentigem die Wunde zu spülen.
Fäden ziehen würde sie in 10 bis 12 Tagen, wobei sie darauf wettete, dass der Hurensohn die Dinger spätestens am siebenten Tag mit seinem Taschenmesser aufzupfen wird.

Insgesamt zeigte sich die New World Order milde interessiert an ihren Fähigkeiten in der Reparatur und Wartung von Bikes wie auch Bikern und unterbreiteten ihr das Angebot bei ihnen einzusteigen, welches sie dankend annahm.  
Sie wusste ja nicht, was auf sie zu kam…

# 2039–heute
## Die Nacht Des Zorns

In den knapp zwei Jahren bei New World Order lernte sie Einiges über Straßen- und Motorradkämpfe, harter handgemachter Musik (und nicht so'n Weichspüler-Simsinn-Drek), Clubpolitik, Extraktion, Sabotage, Schmuggel, Drogenhandel (also echte Drogen, nicht so'n synthetischen Chiphead-Drek) und auch Wetwork – nur, dass es damals nicht Extraktion sondern Entführung und nicht Wetwork sondern Mord hieß.  
Nichts davon bereitete sie allerdings auf die Straßenkrawalle vor, die am 7. Februar 2039 auf sie hereinbrachen.

Neider innerhalb der New World Order, die ihren rasanten Aufstieg miterlebt hatten, waren nur allzu eifrig gewillt, ihren knochigen Elfenarsch im Hafen brennen zu sehen.
Auch wenn Augenzeugen behaupten, dass dieses Ausmaß zu dem Zeitpunkt noch nicht zu erahnen gewesen sei.  
Jedenfalls schien sie die Lunte zu riechen.
"Mein Servo-Motor des rechten Ringfingers unten zuckte, das ist nie ein gutes Zeichen!" kommentiert sie ihre Eingebung heute vor ihren Vertrauten.  
Statt am hektisch vereinbarten Treffpunkt aufzutauchen donnerte sie ihren Komlink in die nächstbeste Mülltonne und raste aus der Stadt so weit ihre Harley sie trug. 

Irgendwo am Rande der NAN Region tauchte sie in einer verlassenen Moonshiner-Hütte unter, die sie aus ihren Schmuggelaufträgen kannte.
Sie musste lediglich zwei armen Hillbillys mittels Motorradkette die Kehlen zudrücken, da ihnen das mit dem "verlassen" wohl nicht ganz klar war.  
Nach einigen Monaten, in denen sie sich mit abgekochtem Regenwasser und selbst gejagtem Fleisch gerade so vor dem Verhungern bewahren konnte (und feststellen musste, dass eine Pistole für die Wildtierjagd echt ungeeignet ist – glücklicherweise gabs in der Hütte wie in einem schlechten Filmklischee eine ordentliche Jagdflinte und hinreichend Munition und sie hatte Zeit zum Üben) traute sie sich zurück in den Sprawl.

Die New World Order war nur noch ein Schatten ihrer selbst, da anscheinend Susanns Komlink in der Mülltonne gefunden und in einer impulsiv-improvisierten Kurzschlussreaktion seitens der Hand Of Five Anhänger kurzerhand das halbe Viertel durch fachgerechte Sprengstoffnutzung dem Erdboden gleich gemacht wurde.  
Zu allem Überfluss sind die Koreaner bei den Yakuza, die nach den Mafia-Kriegen beim Wiederaufbau helfen sollen, für ihren Geschmack etwas zu übereifrig in der Übernahme von Straßengeschäften.  
Es standen also einige Grenzen klärende Unternehmungen auf dem Programm…

Knapp zwei Jahre später kommt ihr die Erfahrung aus 2040 mit der Beseitigung von Koreanern zu Gute, als sie vom Gerücht der Säuberungsaktion seitens Japan hört und kurzerhand beschließt unentgeltlich unter die Arme zu greifen.
Unglücklicherweise (für die Yaks) konnte sie Japaner und Koreaner nicht so richtig auseinander halten.  
Sehen halt alle gleich aus, diese Äffchen.

Ansonsten versuchte sie, als rechte Hand des Babos die New World Order am Laufen zu halten, was über die Jahre immer schwieriger wurde.
Einerseits gabs da das Clubinterne Mimimi zur Aufgabenverteilung und -ausrichtung, andererseits wuchs die Konkurrenz im Straßengeschäft durch eine aus Los Angeles rüberdiffundierte und leider wesentlich besser organisierte Gang namens Cutters stark an.  
Entsprechend war das Tagewerk der Clanorganisation immer wieder unterbrochen durch Schläge und Gegenschläge von den oder gegen die Cutters.
Es war zum Aus-Der-Elfenhaut-Fahren…

## Bye Bye New, Hello Ancient

Abhilfe schaffte dann ganz unerwartet 2052 die Tir Tairngire, die den Hafen Seattles als Umschlagsplatz beanspruchte.  
Genauer gesagt die Ancients, die als stille Wegbereiter dieser Partnerschaft angesehen werden, jedenfalls von sich selbst.

Der Aufnahmeritus war kein allzu großes Problem für "Sapphire", wie sie ob ihrer Vorliebe für einseitig entspiegeltes Saphirglas auf den Motorrandanzeigen abwertend genannt wurde.
Schließlich hatte sie die letzten fünf Jahre mit Bike Combat verbracht.  
Leider brachte ihr dieser Zusammenschluss nicht nur größere Probleme mit den Cutters, sondern dummerweise auch noch neue Probleme mit den Spikes ein.

So durfte sie im Vergleich zu früher weniger Zeit mit Bike Combat und mehr Zeit mit Bike-Combatant-Zusammenflicken verbringen.  
Kleinere politische Aktionen gegen CAS, Hand Of Five oder andere elfenfeindliche Vereinigungen lockerten das Sanitäterinnenleben ein wenig auf.
Zwischendrin verdingte sie sich immer mal wieder als Söldnerin in Guerillakämpfen gegen kleinere Ziele von Aztechnology, einfach weil die seit 2051 sowieso alle (unterhalb eines gewissen Kontostandes) hassten.  
Gerüchten zufolge war sie auch 2055 bei einigen "terroristischen Anschlägen gegen die Universelle Bruderschaft in Seattle" dabei, allerdings dementiert sie diese Gerüchte.
Wer kann schon etwas gegen Elfenfreunde haben, nur weil die sich komische Haustiere halten?  
Die reißen auch beim Ableben keine Astralrisse in den Himmel, wie ein gewisser Präsident es tat…

Mit ihrem gelegentlichen Nebenerwerb verdiente sie immerhin genug, um sich ein halbwegs brauchbares Neurales Rigger Interface einsetzen zu lassen.
Eine Datenbuchse für die Riggerverbindung und eine als Speichererweiterung wurden ihr kurzerhand in den Schädel integriert, obwohl sie nach dem PISS und dem Rigger Interface einfach keine Lust auf noch mehr Chrom im Kopfbereich hatte.  
Auch ihr Lamentieren "Ich bin Ärztin!" brachte den Street Doc nicht von seinem Standpunkt ab, das lange Übertragungswege ein hohes Fehlerpotential darstellten.  
Langsam aber sicher färbte die Arroganz dieser Go-Gang auf sie ab…

Die Validität ihres Ausspruchs konnte sie dann 2059 in den Redmond Barrens unter Beweis stellen, als der Suborbitalflug 1118 aus Tokio da unsanft landete.  
Wobei sie nur die Personen am äußersten Rand des Geschehens mit leichten Splitterverletzungen und starkem Trauma verarzten musste.
Der zusammengedrückte Metallbrei unter dem Brandherd wirkte auf sie nicht so, als gäbe es da noch irgendwas zu flicken, höchstens zu fleddern.

Weil es sie 2058 unerklärlich im unteren Servomotor des rechten Ringfingers zuckte, unterließ sie Versuche, ihren Teil am Unterweltkrieg beizutragen.  
Sie deutete das Zucken als Warnung, dass das ein bis fünf Nummern zu groß für sie sei.
Manchmal, aber nur manchmal, zweifelt sie diese Deutung an und wünscht sich, sie hätte doch damals ein paar Dutzend Yaks gegrillt…

Da half es, dass das in direkter Nachbarschaft befindliche **Crusher 495** ab und an von den Yaks ungewollt besucht wurde und Clubfreunde wie sie entsprechende Gegenstippvisiten durchführten.  
So war das Leben in den Redmond Barrens.

## Heute

Nach 8 Jahren Vollzeit-Ancient-Go-Gang und halbjährlichen bis quartalsweisen kleinen Runs hat sich die kleine Elfe zwei ziemlich gute Standbeine erarbeitet.  
Vor Allem die Arbeit mit den Ancients gibt ihr das Gefühl, etwas von dem erfahrenen Leid aus ihrer Vergangenheit wieder gutmachen zu können.
Dabei bemerkt sie kaum, dass sie demselben Rassismus gegen *homo sapiens sapiens* verfallen ist, der ihr vor über 30 Jahren entgegen gebracht wurde.  
Bis dato kann sie Go-Gang Leben und Runner-Dasein voneinander trennen und bei Letzterem auch mit (oder trotz?) Äffchen den Auftrag zur Zufriedenheit Mr. Johnsons abschließen ohne sie alle gleich umzubringen.
Was drei Nächte später auf der Straße passiert, behält die Straße für sich…

Aus dem simplen Überstechen ihres Brandings entbrannte eine Art Tattoo-Leidenschaft.  
Zwar bemühte sie sich immer, sichtbare Stellen wegen der Identifizierbarkeit blank zu lassen, so ganz hat das allerdings nicht geklappt.
Auch mit Lederjacke sind die Ausläufe ihres "Eulenarms" zu erkennen, Schwanzfedern eines kleinen Kauzes, der im Dunkel der Nacht mit anderen Eulenartigen in grünen Highlights tätowiert ihren Arm hinauf jagt.  
Als Profi trägt sie bei Aufträgen immer ihre für Ancients untypischen befingerten Lederhandschuhe, die einerseits diese Federn mehr als vollständig bedecken, andererseits auch etwaige verräterische Reflexionen ihres Chrom-Arms verhindern.  
Bisweilen vergisst sie bei Vorbereitungen allerdings ihre Handschuhe, und spätestens bei Notoperationen ist ihr auch in den Schatten die Hygiene wichtiger als die Heimlichkeit.

Die Zunahme von Drohnen jedweder Art der letzten Dekaden machen ihr zu Schaffen.
Das sind echte kleine Konzentrations-Killer, wie so ne Mücke in Deinem Zimmer, wenn Du nach nem Run todmüde (aber nicht tot!) versuchst in den Schlaf zu kommen, das Dreks-Viech aber immer um Dich herum schwirrt: nah genug um zu nerven, doch gerade genau so weit weg, dass Du nicht mal mit dem 40er-Jahre-Voll-Chrom-Cyberarm ran kommst…

Wie es sich für eine Ancient gehört, hat sich ihr Fuhrpark weiter entwickelt.  
Lady Starlight ist ihre Yamaha Rapier in mitternachtsblauem Mehrschicht-Effekt-Lack, der beim Einfall der bescheidenen Straßenlaternen Seattles tiefer liegende grüne Sterne aufblitzen lässt.
Zugegebenermaßen sieht das Ding in der prallen Sonne eher lächerlich aus, aber die gibt es in Seattle ja zum Glück eher selten.  
Big Ol' Betty ist ihre Harley-Davidson Scorpion, die sie für übliche Botenfahrten über längere Strecken nutzt.
Relativ unauffällig auf den Straßen von Seattle und damit genau das Richtige für sie.  
Und natürlich Pocahontas, ihr kaffeebrauner Dodge Scoot mit schwarzer Randlackierung für Straßenbewegungen, die lieber ganz unauffällig sind.

Dann sind da noch die beiden äußerlich nahezu identischen Ares Roadmaster, von denen einer als Werkzeugladen umgebaut ist, der Andere allerdings als Transportmittel für Runnerteams genutzt wird.

