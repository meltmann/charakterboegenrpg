# Infos

- Rasse: Elf (C)
- Karmapool
- Gutes Karma
- Reputation

# Attribute (D: 21)

## Vergeben

| Attribut | Basis | Real |
| :------- | ----: | :--- |
| Schnelligkeit | 5 | 6 |
| Intelligenz | 6 | 6  |
| Stärke | 4 | 4 |
| Willenskraft | 4 | 4 |
| Charisma | 1 |  3 |
| Konstitution | 1 | (2) |


## Errechnet

| Attribut | Real |
| :------- | :--- |
| Essenz | 0.5 |
| Reaktion | 6 |
| Initiative |  |
| Magie | — |


# Würfelpools

- Kampfpool
- Initiantenpool

# Zustandsmonitor

## Geistig

## Körperlich

# Aktionsfertigkeiten (A: 50)

## Kampf (22)

| Fertigkeit | Attribut | Stufe |
| :--------- | :------: | ----: |
| Knüppel | Stärke | 5 |
| Pistolen |  Schnelligkeit | 7 |
| such! | Pistole | +1 |
| Peitschen | Schnelligkeit | 6 |
| Modifizierte Motorradkette | Peitschen | +1 |

## Tech (12)

| Fertigkeit | Attribut | Stufe |
| :--------- | :------: | ----: |
| Bauen/Reparieren | Intelligenz | 6 |
| BioTech | Intelligenz | 6 |

## Reaktion (14)

| Fertigkeit | Attribut | Stufe |
| :--------- | :------: | ----: |
| Motorrad | Reaktion | 6 |
| Yamaha Rapier | Motorrad | +1 |
| Auto | Reaktion | 6 |
| Ares Roadmaster | Auto | +1 |


# Wissensfertigkeiten (30)

| Straßenwissen | Wert | Zusatz |
| :------------ | ---: | :----: |
| Gang-Identifizierung | 5 | Ancients |
|  | 3 | Cutters |
| Schmuggelrouten | 4 | Seattles Landesgrenzen |
| Schmugglerverstecke | 3 | Ländliche Gegenden |

| Akademisches | Wert | Erwerb |
| :----------- | ---: | :----: |
| Medizin/Anatomie | 5 | Studium |
| Chemie | 2 | Praktikum Anästhesie |

| Interessen | Wert |
| :--------- | ---: |
| Troll-Trash-Metal-Bands | 4 |
| Combat Biking | 4 |

## Sprachen (9)

| Sprache | Reden/Hören | Lesen/Schreiben |
| :------ | ----------: | --------------: |
| Englisch | 7 | 3 |
| Sperethiel | 2 | – |


# Bioware/Cyberware (190.000¥)

| Ware | Anbringung | Essenz | Preis |
| :--- | :--------: | :----: | ----: |
| Chipbuchse | Linke Schläfe | 0.2 | 1.000¥ |
| Memory | 90 MP | 0.3 | 13.5000 |
| Datenbuchse | Rechte Schläfe | 0.2 | 1.000¥ |
| Offensichtlicher Cyberarm | Rechts, Gebraucht | 1 | 37.000¥ |
| Eingebaute Smartgunverbindung | Cyberarm | 0.25 | 2.500¥ |
| Erhöhte Schnelligkeit +1 | Cyberarm | — | 50.000¥ |
| Kompositknochen | Alumium | 1.15 | 25.000¥ |
| Riggerkontrolle | Stufe 2 | 2.4 | 60.000¥ |


# Fahrzeuge (179.000¥)

| Karre | Addon | Preis |
| :---- | :---: | ----: |
| Dodge Scoot | | 2.000¥ |
| Harley-Davidson Scorpion | | 14.300¥ |
| Yamaha Rapier | | 12.700¥ |
| | Nitro-Injektor Stufe 1 | 3.500¥ |
| Ford-Canada Bison | | 145.000¥ |
| | Datenbuchse | 2.500¥ |
| | Fahrzeugadapter | 2.500¥ |



# Panzerung (1.900¥)

- Synthleder Ancient Komplettsatz (250¥)
- Freizeitleder Echt (750¥)
- Panzerjacke (900¥)
- Gute Ausgehkleidung (1.000¥)
- Arbeitskleidung (500¥)



# Waffen (1.150¥)

- Waffenlos: Stärke + 3 M Betäubung (Aluminiumknochen)
- Pistole: Beretta Model 101 (350¥)
- Halbautomatisch: Ingram Smartgun (950¥)
- Modifizierte Motorradkette, improvisiert: Stärke S Echt

- Standardmunition 10 Schuss á 20¥, Leicht, 50 Magazine (1.000¥)
- Standardmunition 10 Schuss á 20¥, Halbautomatisch, 100 Magazine (2.000¥)
- Gelmunition 10 Schuss á 30¥, Leicht, 30 Magazine (900¥)

# Handycaps

- Drahtlos-Desorientierung
- Leichte Drohnenphobie
- Mittlere Paranoia


# Connections (15.000¥)

- Green Lucifer, Leiter Ancients, Stufe 2 (gratis)
- Janus Kosky, Inhaber Crusher 495, Stufe 2 (10.000¥)
- Skipper, Werkstattinhaber und Schieber, Stufe 1 (5.000¥)


# Lebensstile (3.600¥)

- Ancients Werkstatt, 100¥/Monat, 6 Monate
- Downtown Schmugglerversteck, 100¥/Monat, 6 Monate
- Puyallup Schmugglerversteck, 100¥/Monat, 6 Monate
- Everett Schmugglerversteck, 100¥/Monat, 6 Monate
- Tacoma Schmugglerversteck, 100¥/Monat, 6 Monate
- Auburn Schmugglerversteck, 100¥/Monat, 6 Monate


## Restguthaben (95¥)

Startkapital: 95¥
