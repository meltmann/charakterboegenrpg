# Susann (Sue) "Sapphire" Jackson

Ihr Straßenname rührt von ihrem persönlichen Tick her, im Fahrgastraum am Liebsten mit einseitig entspiegeltem Saphirglas eingefasste Instrumente zu haben.

Einer der Ersatzteillieferanten stellte sich als Schieber heraus und verpasste ihr einen "kleinen Run", in dem ein Kon den Yakuza ein wenig auf die Finger klopfen wollte.  

Sapphire nahm an und ließ sich für die Kreds eine Riggereinheit zusätzlich zum Cyberarm implantieren.  
Seither ist sie versucht, jede Karre zu fahren. Egal wie viele oder wenige Räder das Ding hat. 
Hauptsache sie kann drin sitzen.
Drohnen sind Spielkram.

Sie war im Auftrag der Ancients an einigen Fahrzeugschmuggeleien beteiligt.  
Für ihre Touren hatte sie immer gut sichtbar eine Stiege SynthBeer auf dem Rücksitz, die haargenau unterhalb der Export- beziehungsweise Importbestimmungen lag.  
Jedes einzelne Mal wurde sie rausgezogen und jedes einzelne Mal wurde ihre Karre auf der Suche nach nur einer einzigen weiteren Flasche komplett zerlegt.
Jedes einzelne Mal ohne Erfolg.  
Erst als einem auffiel, dass sie morgens in einem hellblauen Eurocar Westwind 2000 rausgefahren und nachmittags in einem blassroten Ford Americar wieder reingefahren kam, hörte sie mit diesen Aufträgen auf.

# Fuhrpark

- Yamaha Rapier (Dank der Ancients)
- Harley Davidson Scorpion (warum auch nicht? Die Tech im Arm macht's möglich)
- Irgend nen billigen Vespa-Roller-Verschnitt, sie will ja nicht auffallen
- Ares Roadmaster mit Werkzeugladenumbau weil wegen Paranoia und so
- Ares Roadmaster, äußerlich identisch, als Runner-Transportmittel

# Waffenarsenal

- Stumpfe Waffen, ein 48er Schraubenschlüssel ist schnell zur Hand
- **Faustfeuerwaffe** *9mm* oder Äquivalent, maximal halbautomatisch, Beretta oder Glock bevorzugt (Nostalgie)
- Vielleicht noch Straßengang-Nahkampfzeugs wie ne Stahlkette oder so? Mal bei Go-Gangern spicken...

# Connections

- Go-Ganger der Ancients (Stufe 2)
    - Diebesgut, Straßengerüchte, Beförderungen

# Sonstiges

- Komplett magielos
- Cyberarm weil Story
- Heilerin
- Mechanikerin
- Riggerdeck
- Waffengrundkenntnisse

