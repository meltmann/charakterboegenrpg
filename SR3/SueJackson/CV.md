# Tabellarischer Lebenslauf

| Persönliche | Daten |
| :-- | --: |
| *Name* | Susann Jackson |
| *Spitzname* | Sue |
| *Straßenname* | Sapphire |
| *Geboren* | Ja. 2011-11-11 in Seattle |
| *Anschrift* | 32nd Avenue, Redmond District, Seattle — und zwar schon erstaunlich lange |
| *Komlink* | so'n Drek hat se nich |
| *LTG-Nummer* | so'n Drek hat se auch nich |
| *Pager* | 555-SAPPHIRE11 |
| (Klartext) | 555-7277447311 |

# Berufliche Laufbahn

## 2052—heute

**Ancients**
Instandhaltung von Elf und Maschine

- Motorrad-Reparaturen
- Erst- und Folgeversorgung von Verletzungen nach Verkehrsunfällen jedweder Art
- Organisation und Durchführung politischer Kundgebungen
- Güterbesitzumverteilungen
- Überführungsgebühreinsparungen

## 2040—2052

**New World Order**
Linke Backe des Babo

- Motorrad-Reparaturen
- Erst- und Folgeversorgung von Verletzungen nach Verkehrsunfällen jedweder Art
- Güterbesitzumverteilungen
- Überführungsgebühreinsparungen
- Betäubungsmittelinverkehrbringungen
- Hoheitsgebietsbeanspruchungen

## 2034—2039

**Transys**
Rettungssanitäterin

- Opferbergungen
- Notoperationen
- Cyberimplantationsvertrieb
- Rettungswageninstandhaltung

# Ausbildung

## 2028—2033

**Transys**
Duales Studium Metahumanmedizin, Abschluss: PhD

- Ganzheitliche Behandlungsmethoden
- Operationstechniken
- Cyberimplantatsverpflanzungen
- Plastische Chirurgie

*PhD Thesis*  
"Die positiven Auswirkungen lokalanästhetischer Zusatzbehandlungen auf die Abstoßungsreaktionen der metahumanen Anatomie gegenüber moderner Cyberimplantate", Transys Laboratory For Meta Human Medicine, im September 2029, Gesamtnote 100/110

## 2021–2027

**Transys EGU–Internat**
Weiterführende Schule

## 2017—2020

**Transys EGU–Internat**
Grundschule

# Kenntnisse und Fähigkeiten

## Verteidungskünste

- Waffenloser Kampf
- Stumpfe Einhandwaffen
- Kettenwaffen
- Kleinkaliberpistolen

## Technik

- Bauen/Reparieren
- Biotech

## Führerschein

- Motorrad
	- Sonderausbildung: Yamaha Rapier
- KFZ bis 3,8 Tonnen

## Sprachkenntnisse

- Englisch 
	- Hören/Sprechen: verhandlungssicher
	- Lesen/Schreiben: kommunikationssicher
- Sperethiel
	- Hören/Sprechen: grundlegende Kenntnisse der Kategorie "Gosse"

# Ehrenamtliche Tätigkeiten

## 2059
Freiwilliger Sanitätereinsatz nach Absturz des Suborbitalflugs 1118

## 2056—2058
*Öffentlichkeitsarbeit* zur Aufklärung der Machenschaften Aztechnology's
## 2055
Politische Aktivitäten zur *Befriedung* der Universellen Bruderschaft in Seattle

## 2042
Unterstützende Tätigkeit zur Neuorientierung der Yakuza nach japanischem Vorbild durch aktive Außerdienststellung koreanisch indoktrinierter Mitglieder

# Hobbies

- Musik
	- Troll Trash Metal
	- Industrial
	- Hard Rock
	- Nova Rock
	- Angst Pop
- Sport
	- Motorsport
	- Mixed Martial Arts
