# Sue Jackson

## Aussehen

- durchschnittlich groß
- dunkeloliver Hautton
- eher schlank und drahtig
- kaum Brustumfang oder erkennbare Taille, was durch ihren Kleidungsstil noch weiter egalisiert wird
- rechter Arm Vollchrom Cyberware "von vor'm Krieg" – vermutlich sogar noch vor den Mafia Kriegen
- linker Arm bis hin zu Teilen des Handrückens und hoch bis zum Halsansatz mit einem in Schwarz-Grau-Grün gehaltenen "Eulenflug bei Nacht" Ärmel zutätowiert
- kurze, kantige Nase
- langes, kantiges Kinn
- hohe Wangenknochen
- sehr dünne, fast geradlinig geformte und dafür verhältnismäßig breite Lippen
- für eine Elfin verhältnismäßig kurze Ohren
- Gesichtsnarben
	- rechte Wange etwa ein Finger breit unter dem Auge, beginnt etwa zwei Finger breit rechts der Nase und zieht sich fast waagerecht fast bis zum rechten Ohr
	- lange, zackige Narbe an der Stirn, etwa zwei Finger breit oberhalb der Augenbrauen
- hellblau gefärbtes, etwa rückenlanges Haar: mal als ausladender Mohawk aufgestellt, mal lieblos zum Zopf zusammengeknüllt, mal gleichgültig offen getragen
- rennt meist komplett in Synth-Leder rum, selbst wenn's nicht die Ancient Robe mit grünem Logo ist
- wenn sie mal ohne Lederjacke angetroffen wird, dann im Monteurs-Overall oder mit irgendwelchen Shirts irgendwelcher Bands, von denen nach 2030 Geborene vermutlich nicht einmal was gehört haben. Oder halt Shirts von The Shadows und JetBlack, sie ist schließlich auch nur ein Elf
- trägt fast immer Handschuhe: Fingerlose in Ancient Montur, die gesamte Hand bedeckende bei Runs, sterile Einmalhandschuhe bei Operationen, privat mal so mal so (nur halt nicht die Einmaldingsda)
- ihre Stimme ist hell, hoch und klar
- ihr Gang fast schon übertrieben aufrecht und stolz, Kategorie "Stock im Arsch"
- zwei Datenbuchsen am Kopf, je eine an der linken und eine an der rechten Seite, relativ symmetrisch angeordnet
- diese versteckt sie oft hinter Plugs mit irgendwelchen Aufdrucken (wie zum Beispiel dem Ancients Logo bei Gangaktivitäten, Bandlogos in Clubs oder wenig reflektierendem, schlicht mattem schwarz auf Runs)

## Psyche

- Latenter Rassismus gegenüber Äffchen (Homo Sapiens Sapiens) im Allgemeinen und Speziellen
- Leichte Phobie vor Drohnen, vor Allem mit Überwachungssystemen
- Abneigung gegen Fernlenkeinrichtungen ("Drahtlos-Desorientierung"), beschreibt sie ihren engen Vertrauten als "das Gefühl aus ihrem eigenen Körper gerissen zu werden"
- Generell allem gegenüber ausgesprochen paranoid
- Speziell Gangs gegenüber aufgeschlossen
- Speziell Nicht-Äffchen gegenüber zurückhaltend freundlich
- Professionell in beruflicher Zusammenarbeit, kann dort ihre Rassenvorurteile hinreichend gut zurückhalten
- Eher lösungsorientiert als problemorientiert: es ist ihr wichtig, dass die Symptome beseitigt werden, die Ursache ist ihr egal. Es sei denn, der Auftrag behauptet das Gegenteil
- Loyal gegenüber engen Vertrauten
- Extrem nachtragend (bis auf's Blut) bei Vertrauensmissbrauch
- Kann eine ganze Menge an Straßenscheiß inklusive Wet Work billigen, wird allerdings zur Furie bei:
	- sexueller Gewalt jedweder Art
	- Zwangsverabreichung von Betäubungsmitteln
- Adrenalinjunkie
- Zweiradjunkie

