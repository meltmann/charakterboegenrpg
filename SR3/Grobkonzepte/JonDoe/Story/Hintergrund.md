# "Jon Doe"

## Was er weiß

Jon arbeitet in einem Waisenhaus, das in einer verlassenen Kirche errichtet wurde.  
Er hat dort eine höhere Stellung mit Personalverantwortung, nimmt sich aber dennoch mehr Zeit als er sollte für die Belange der Kinder.
Von denen einige ganz eindeutig SIN-los geboren worden waren.

Jon ist ein ausgesprochen religiöser Mensch.
Allerdings fühlt er sich eher dem Äquivalent des Alten Testaments verbunden, also dem ganzen Geseier von Gleichheit, Auge um Auge und so Zeugs.

Die Kirche befindet sich auf einem wirtschaftlich interessanten "Filetgrundstück" umringt von signifikant größeren Megaplexen und ist daher für die Konzerne interessant.
Das Gebäude steht zwar unter Denkmalschutz, doch der war zeitlich begrenzt und läuft demnächst aus.
Da sich zufälligerweise die gesetzlichen Vorgaben zur Bestimmung der Verlängerung des Denkmalschutzes in den letzten Monaten drastisch verschärft haben, ist es mehr als fraglich, ob diese Kirche weiterhin unter diesen Schutz fällt.  
"Unglücklicherweise" wird das Waisenhaus allerdings von einer Stiftung betrieben, die das Gebäude auf Lebenszeit pachten konnte und die Besitzverhältnisse so geschickt aufteilen konnte, dass die Kosten für entsprechende Bestechungs-, Entmündigungs- und Verschwindeaktivitäten gegen die Eigentümer:innen in keinem Verhältnis zum Nutzen stehen.  
Noch…

Wenn ab und zu mal jemand auf dem Markt unvorsichtig ist, kurz unbeaufsichtigt Einkäufe vor der Tür stehen lässt oder anderweitig Güter ohne Besitz erscheinen, sorgt Jon für eine gerechte Umverteilung unter den Waisen.
Der Herr gibt, der Herr nimmt…  
Als die behördliche und unternehmerische Auflagen an das Waisenhaus gleichermaßen immer stärker stiegen, so dass die Kosten allein über staatliche Zuschüsse und Spenden kaum noch aufzubringen waren, suchte Jon zum ersten Mal nach alternativen Geldmittelbeschaffungswegen.  

Jon entstammt den indigenen Völkern und kann diese Tatsache durch sein Äußeres kaum verbergen. 
Obwohl er in Seattle geboren ist und mit der NAN absolut nichts am Hut hat, ist sein Leben von Anfeindungen geprägt.  
Entsprechend hat er diverse Erfahrungen machen dürfen, die über fremdenfeindliche Worte und Aktivitäten bis hin zur rechtswidrigen Konspiration von Lebensmitteln aus Abfallbehältern zum nackten Überleben reichen.
Auf den Straßen versteckt er sich daher gern in einem knöchellangen braunen Mantel mit Kapuze, die er meist zwecks Verdeckung der Gesichtszüge bis über die Augen zieht.

## Was er nicht weiß

Er ist tiefer mit den NAN verwoben, als ihm bewusst ist.  
Seine Religion ist lediglich sein Verständnis des ihm zugeteilten Waschbär-Geistes.  
In einem Schließfach irgend einer Bank liegt ein Familien-Wappenring seiner Großmutter, der mehrere Mäuse zeigt, welche aneinander geschmiegt und leicht verschlungen in unterschiedliche Richtungen schauen.  
Die schamanistische Magie übersprang seine Eltern und manifestierte sich in ihm, doch hält er seinen "inneren Kompass", wie er seine Religion beschreibt, für das göttliche Wirken einer übermenschlichen Entität. Nicht für das Herumwuseln eines Waschbär-Geistes…
